import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';
import { Giria } from '../entidades/giria';

@Component({
  selector: 'app-girias',
  templateUrl: './girias.component.html',
  styleUrls: ['./girias.component.scss']
})
export class GiriasComponent implements OnInit {

  girias: Giria[] = [];

  constructor(private api: APIService) { }

  ngOnInit() {
    this.getGirias();
  }
  
  getGirias() {
    this.api.listarGirias().subscribe(data => this.girias = data);
  }
}
