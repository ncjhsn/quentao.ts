import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiriasComponent } from './girias.component';

describe('GiriasComponent', () => {
  let component: GiriasComponent;
  let fixture: ComponentFixture<GiriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
