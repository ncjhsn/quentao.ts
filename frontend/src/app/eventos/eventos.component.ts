import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.scss']
})
export class EventosComponent implements OnInit {

  eventos: Array<any>;

  constructor(private api: APIService) { }

  ngOnInit() {
    this.geteventos();
  }

  geteventos() {
    this.api.listarEventos().subscribe(data => this.eventos = data);  
  }
}
