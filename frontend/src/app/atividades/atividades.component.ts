import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';
import { Evento as Atividade } from '../entidades/evento'
import { Cupinxa } from '../entidades/cupinxa'
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-atividades',
  templateUrl: './atividades.component.html',
  styleUrls: ['./atividades.component.scss']
})
export class AtividadesComponent implements OnInit {
  
  atividade: Atividade;
  panelOpenState = false;
  atividades: Atividade[] = [];
  cpxs: Cupinxa[] = [];
  subs: Subscription;
  valor: string = '';
  constructor(private api: APIService) { }

  ngOnInit() {
    this.listarAtividades();
    this.cupinxas();
  }


  listarAtividades() {
    this.api.listarAtividades().subscribe(data => {
      this.atividades = data;

    });
  }

  cupinxas() {
    this.api.listarCpx().subscribe(data => this.cpxs = data);
  }

}
