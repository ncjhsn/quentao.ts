import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtividadesComponent } from './atividades/atividades.component';
import { CupinxasComponent } from './cupinxas/cupinxas.component';
import { EventosComponent } from './eventos/eventos.component';
import { GiriasComponent } from './girias/girias.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: 'atividades', component: AtividadesComponent},
  {path: 'cupinxas', component: CupinxasComponent},
  {path: 'eventos', component: EventosComponent},
  {path: 'girias', component: GiriasComponent },
  {path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
