import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Evento } from './entidades/evento';
import { Cupinxa } from './model/cupinxa';
import { Giria } from './entidades/giria';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  PORT = 7777;
  path = `http://localhost:${this.PORT}`;
  urlAtividades = `${this.path}/atividades`;
  urlCpx = `${this.path}/cupinxas`;
  urlEventos = `${this.path}/eventos`;
  urlGirias = `${this.path}/girias`;
  urlCadCpxs = `${this.path}/cadastrarCupinxa`;

  constructor(private http: HttpClient) { }

  listarAtividades() {
    return this.http.get<Evento[]>(this.urlAtividades);
  }

  listarCpx() {
    return this.http.get<Cupinxa[]>(this.urlCpx);
  }

  listarEventos() {
    return this.http.get<Evento[]>(this.urlEventos);
  }

  listarGirias() {
    return this.http.get<Giria[]>(this.urlGirias);
  }

  cadCpx(cpx: Cupinxa) {
    const httpOptions= {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
    return this.http.post(this.urlCadCpxs, cpx, httpOptions );
  }


}
