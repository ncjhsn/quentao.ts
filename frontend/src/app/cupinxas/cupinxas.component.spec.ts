import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CupinxasComponent } from './cupinxas.component';

describe('CupinxasComponent', () => {
  let component: CupinxasComponent;
  let fixture: ComponentFixture<CupinxasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CupinxasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CupinxasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
