import { Component, OnInit, OnDestroy } from '@angular/core';
import { APIService } from '../api.service';
import { Cupinxa } from '../model/cupinxa';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cupinxas',
  templateUrl: './cupinxas.component.html',
  styleUrls: ['./cupinxas.component.scss']
})
export class CupinxasComponent implements OnInit {

  cpxs:Cupinxa[] = [];
  cpx: Cupinxa;
  subs: Subscription;

  constructor(private api: APIService) { }

  ngOnInit() {
    this.cupinxas();
    this.cpx = { idade: 0, nome: '' };
  }

  onSubmit() {
    this.subs = this.api.cadCpx(this.cpx).subscribe(() => { this.cpx.idade = 0; this.cpx.nome = '' });
  }

  
  cupinxas() {
   this.api.listarCpx().subscribe(data => this.cpxs = data);
  }
 
  // ngOnDestroy() {
  //   this.subs.unsubscribe();
  //   this.subs2.unsubscribe();
  // }
}
