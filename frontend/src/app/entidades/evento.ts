import { Cupinxa as cpx } from '../entidades/cupinxa'

export interface Evento {
    local: string,
    data?: Date,
    hora?: Date,
    participantes?: cpx[]
}