import { Cupinxa } from '../entidades/cupinxa'
import { CupinxaModel } from './cupinxaModel'

export class CupinxaRepositorio { 
    static async cadastrar (cupin: Cupinxa): Promise<Cupinxa> {
        return CupinxaModel.create(cupin)
     }

     static async listarCpx(): Promise<Cupinxa []> { 
         return CupinxaModel.find().exec()
     } 

     static async alterarNome(nome: string, novoNome: string) {
         return CupinxaModel.updateOne({nome: nome}, {nome: novoNome})
     }

     static async buscarPorNome(nome: string): Promise<Cupinxa | null> { 
         return CupinxaModel.findOne().where('nome').equals(nome).exec()
     }
}