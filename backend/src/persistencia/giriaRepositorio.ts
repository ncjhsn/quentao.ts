import { giriaModel } from './giriaModel'
import { Giria } from '../entidades/giria'

export class GiriaRepositorio { 
    static async cadastrar(giria: Giria): Promise<Giria> { 
        return giriaModel.create(giria)
    }

    static async listar(): Promise<Giria []> { 
        return giriaModel.find().exec()
    }

    static async buscarGiriaPorNome(nomeGiria :string): Promise<Giria | null> { 
        return await giriaModel.findOne().where('nome').equals(nomeGiria)
    }
}