import { Cupinxa as Cpx } from '../entidades/cupinxa'
import { Schema, Document, model, Model } from 'mongoose'

interface CupinxaDocument extends Cpx, Document { }

const CupinxaSchema = new Schema({
    nome: { type: String, required: true },
    idade: { type: Number, required: true, min: 18 }
})

export const CupinxaModel: Model<CupinxaDocument> = model<CupinxaDocument>(
    'Cupinxa', CupinxaSchema, 'cupinxada'
)