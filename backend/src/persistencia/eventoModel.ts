import { Evento } from '../entidades/evento'
import { Schema, Document, model, Model, SchemaTypes } from 'mongoose'

interface EventoDocument extends Evento, Document { }

const eventoSchema = new Schema({
    local: { type: String, required: true },
    data: { type: Date },
    hora: { type: Date },
    id: {type: Number},
    participantes: [{type: SchemaTypes.ObjectId, ref: 'cupinxada' }]
})

export const eventoModel: Model<EventoDocument> = model<EventoDocument>(
    "Evento", eventoSchema, 'eventos'
)