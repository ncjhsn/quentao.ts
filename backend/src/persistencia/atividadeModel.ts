import { Evento } from '../entidades/evento'
import { Schema, Document, model, Model, SchemaTypes } from 'mongoose'

interface EventoDocument extends Evento, Document { }

const eventoSchema = new Schema({
    local: { type: String, required: true },
    data: { type: Date },
    hora: { type: Date },
    id: {type: Number},
    participantes: [{type: SchemaTypes.ObjectId, ref: 'Cupinxa', required: true }]
})

export const atividadeModel: Model<EventoDocument> = model<EventoDocument>(
    "Atividade", eventoSchema, 'atividades'
)