import { Evento } from '../entidades/evento'
import { eventoModel } from './eventoModel'
import { CupinxaModel } from './cupinxaModel';
import { Cupinxa } from '../entidades/cupinxa';

export class eventoRepositorio {
    static async cadastrar(evento: Evento, cpx: Cupinxa): Promise<Evento> {
        const arrCpx: Cupinxa [] = [];
        arrCpx.push(cpx)
        const eventoCriado = await eventoModel.create({
            local: evento.local,
            data: evento.data,
            hora: evento.hora,
            id: await eventoModel.find().count() + 1,
            participantes: arrCpx
        })
        return eventoCriado;
    }

    static async listar(): Promise<Evento[]> {
        return eventoModel.find().populate('participantes', CupinxaModel).exec()
    }

    static async editarData(idEvento: number, novaData: Date): Promise<void> {
        return eventoModel.updateOne({ id: idEvento }, { data: novaData })
    }

    static async editarHora(idEvento: number, novaHora: Date): Promise<void> {
        eventoModel.updateOne({ id: idEvento }, { hora: novaHora })
    }

    static async editarLocal(idEvento: number, novoLocal: string): Promise<void> {
        eventoModel.updateOne({ id: idEvento }, { local: novoLocal })
    }

    static async remover(idEvento: number): Promise<void> {
        eventoModel.deleteOne({ id: idEvento })
    }
}

