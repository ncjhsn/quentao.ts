import { Evento as Atividade } from '../entidades/evento'
import { atividadeModel } from './atividadeModel'
import { CupinxaModel } from './cupinxaModel';
import { Cupinxa } from '../entidades/cupinxa';

export class atividadeRepositorio {
    static async cadastrar(atividade: Atividade, cpx: Cupinxa): Promise<Atividade> {
        const arrCpx: Cupinxa [] = [];
        arrCpx.push(cpx)
        const atividadeCriada = await atividadeModel.create({
            local: atividade.local,
            data: atividade.data,
            hora: atividade.hora,
            id: await atividadeModel.find().countDocuments() + 1,
            participantes: arrCpx
        })
        return atividadeCriada;
    }

    static async buscarPorId(id: number): Promise<Atividade | null>{
        return atividadeModel.findOne().where('id').equals(id).populate('participantes', CupinxaModel).exec()
    }

    static async buscarPorLocal(local: string): Promise<Atividade | null> {
        return atividadeModel.findOne().where('local').equals(local)
    }

    static async listar(): Promise<Atividade[]> {
        return atividadeModel.find().populate('participantes', CupinxaModel).exec()
    }

    static async editarData(idAtividade: number, novaData: Date): Promise<void> {
        return atividadeModel.updateOne({ id: idAtividade }, { data: novaData })
    }

    static async editarHora(idAtividade: number, novaHora: Date): Promise<void> {
        atividadeModel.updateOne({ id: idAtividade }, { hora: novaHora })
    }

    static async editarLocal(idAtividade: number, novoLocal: string): Promise<void> {
        atividadeModel.updateOne({ id: idAtividade }, { local: novoLocal })
    }

    static async remover(idAtividade: string): Promise<void> {
        atividadeModel.findByIdAndDelete(idAtividade)
    }
}

