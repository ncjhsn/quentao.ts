import { Giria } from '../entidades/giria'
import { Document, Schema, model, Model } from 'mongoose'

interface GiriaDocumento extends Giria, Document { }

const giriaSchema: Schema = new Schema({
    palavra: { type: String, required: true },
    definicao: { type: String }
})

export const giriaModel: Model<GiriaDocumento> = model<GiriaDocumento>(
    'Giria', giriaSchema, 'girias'
)