import { Router } from 'express';
import * as QuentaoController from '../controlador/quentao.controller';

export const router = Router();

router.get('/girias', QuentaoController.getGirias);
router.post('/girias', QuentaoController.postCadastrarGiria);