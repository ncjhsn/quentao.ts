import { Router } from 'express';
import * as QuentaoController from '../controlador/quentao.controller';

export const router = Router();

router.get('/cupinxas', QuentaoController.getCpx);
router.post('/cadastrarCupinxa', QuentaoController.postCadastrarCupin);