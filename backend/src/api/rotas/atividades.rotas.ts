import { Router } from 'express';
import * as QuentaoController from '../controlador/quentao.controller';

export const router = Router();

router.get('/atividades', QuentaoController.getAtividades);
router.post('/atividades', QuentaoController.postCadastrarAtividade);
