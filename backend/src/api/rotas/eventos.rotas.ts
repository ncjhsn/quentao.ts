import { Router } from 'express';
import * as QuentaoController from '../controlador/quentao.controller';

export const router = Router();

router.get('/eventos', QuentaoController.getEventos);
router.post('/eventos', QuentaoController.postCadastrarEvento);