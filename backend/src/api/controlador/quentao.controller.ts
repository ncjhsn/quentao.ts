import { Request, Response, NextFunction } from 'express';

import { Quentao } from '../../negocio/quentao';

export const getAtividades = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const atividades = await Quentao.listarAtividades();
        res.json(atividades);
    } catch (error) {
        next(error);
    }
};

export const getEventos = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const eventos = await Quentao.listarEventos();
        res.json(eventos);
    } catch (error) {
        next(error);
    }
};

export const getGirias = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const girias = await Quentao.listarGirias();
        res.json(girias);
    } catch (error) {
        next(error);
    }
}

export const getCpx = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const cpxs = await Quentao.listarCupinxas();
        res.json(cpxs);
    } catch (error) {
        next(error);
    }
}


// Posts
export const postCadastrarAtividade = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const obj = req.body;
        console.log(obj);
        await Quentao.cadastrarAtividade(obj.atividade, obj.atividade.cpx_nome);
        res.send(`Atividade ${obj.atividade.local} cadastrada por ${obj.atividade.cpx_nome}`);
    } catch (error) {
        console.log('brete');
        next(error);
    }
}

export const postCadastrarCupin = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const obj = req.body;
        await Quentao.cadastrarCpx(obj);
        res.send(`${obj.nome} foi cadastrado!`);
    } catch (error) {
        next(error);
    }
}

export const postCadastrarEvento = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const obj = req.body;
        await Quentao.cadastrarEvento(obj.evento, obj.cpx_nome);
        res.send(`Atividade ${obj.atividade.local} cadastrada por ${obj.cpx_nome}`);
    } catch (error) {
        next(error);
    }
}

export const postCadastrarGiria = async (req: Request, res: Response, next: NextFunction) => {
    try{ 
        const obj = req.body;
        await Quentao.cadastrarGiria(obj);
        res.send(`Giria ${obj.palavra} cadastrada!`);
    } catch (error) { 
        next(error);
    }
}

export const postParticiparAtividade = async (req: Request, res: Response, next: NextFunction) => { 
    try{
        const obj = req.body;
        await Quentao.participarAtividade(obj.cpx, obj.id_atividade);
        res.send(`${obj.cpx} se juntou a atividade`);
    } catch (error) { 
        next(error);
    }
}

export const postParticiparEvento = async (req: Request, res: Response, next: NextFunction) => { 
    try{
        const obj = req.body;
        await Quentao.participarEvento(obj.cpx, obj.id_evento);
        res.send(`${obj.cpx} se juntou ao evento`);
    } catch (error) { 
        next(error);
    }
}
