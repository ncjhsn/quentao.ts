import { Evento } from "../entidades/evento";
import { Evento as Atividade } from '../entidades/evento'
import { eventoRepositorio } from "../persistencia/eventoRepositorio";
import { atividadeRepositorio } from "../persistencia/atividadeRepositorio";
import { Cupinxa } from "../entidades/cupinxa";
import { CupinxaRepositorio } from "../persistencia/cupinxaRepositorio";
import { atividadeModel } from "../persistencia/atividadeModel";
import { GiriaRepositorio } from "../persistencia/giriaRepositorio";
import { Giria } from "../entidades/giria";

export class Quentao {

    static async cadastrarEvento(evento: Evento, nome_cpx: string): Promise<Evento | undefined> {
        const cpx = await CupinxaRepositorio.buscarPorNome(nome_cpx)
        if(cpx !== null) { 
            return eventoRepositorio.cadastrar(evento, cpx)
        } else { 
            console.log('cupin não encontrado')
            return
        }
    }

    static async cadastrarGiria(giria: Giria): Promise<Giria> {
        return await GiriaRepositorio.cadastrar(giria)
    }

    static async listarGirias(): Promise<Giria[]> {
        return await GiriaRepositorio.listar()
    }

    static async buscarGiriaPorNome(nomeGiria: string): Promise<Giria | null> {
        return GiriaRepositorio.buscarGiriaPorNome(nomeGiria)
    }

    static async listarEventos(): Promise<Evento[]> {
        return await eventoRepositorio.listar()
    }

    static async cadastrarAtividade(atividade: Atividade, cpx_nome: string): Promise<Atividade | undefined> {
        const cpx = await CupinxaRepositorio.buscarPorNome(cpx_nome)
        if (cpx !== null) {
            return await atividadeRepositorio.cadastrar(atividade, cpx)
        } else {
            console.log('cupinxa não encontrado')
            return
        }
       
    }

    static async listarAtividades(): Promise<Atividade[]> {
        return await atividadeRepositorio.listar()
    }


    static async cadastrarCpx(cpx: Cupinxa): Promise<Cupinxa> {
        return await CupinxaRepositorio.cadastrar(cpx)
    }

    static async listarCupinxas(): Promise<Cupinxa[]> {
        return await CupinxaRepositorio.listarCpx()
    }

    static async participarAtividade(nome: string, idAtividade: number) {
        try {
            const cpx: Cupinxa | null = await CupinxaRepositorio.buscarPorNome(nome)
            if (cpx !== null) {
                let ativ: Atividade | null = await atividadeRepositorio.buscarPorId(idAtividade)

                if (ativ !== null) {
                    let listaCpx: Cupinxa[] | undefined = ativ.participantes;
                    listaCpx!.push(cpx)
                    await atividadeModel.updateOne({ id: idAtividade }, { participantes: listaCpx })
                    return await atividadeRepositorio.listar()
                }
            }
        } catch (err) {
            console.log(err.message)
        }
    }

    static async participarEvento(nome: string, idEvento: number) {
        try {
            const cpx: Cupinxa | null = await CupinxaRepositorio.buscarPorNome(nome)
            if (cpx !== null) {
                let evento: Evento | null = await atividadeRepositorio.buscarPorId(idEvento)

                if (evento !== null) {
                    let listaCpx: Cupinxa[] | undefined = evento.participantes;
                    listaCpx!.push(cpx)
                    await atividadeModel.updateOne({ id: idEvento }, { participantes: listaCpx })
                    return await atividadeRepositorio.listar()
                }
            }
        } catch (err) {
            console.log(err.message)
        }
    }

}