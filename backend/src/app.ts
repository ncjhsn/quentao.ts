import express from 'express';
import cors from 'cors';
import { router as atividadeRouter } from './api/rotas/atividades.rotas';
import { router as cpxRouter } from './api/rotas/cupinxas.rotas';
import { router as eventosRouter } from './api/rotas/eventos.rotas';
import { router as giriaRouter } from './api/rotas/girias.rotas';
import bodyParser from 'body-parser';

const app = express();
const path = '';

app.set('port', 7777);
app.use(cors());
app.use(bodyParser.json());
app.use(path, atividadeRouter);
app.use(path, cpxRouter);
app.use(path, eventosRouter);
app.use(path, giriaRouter);

export default app;