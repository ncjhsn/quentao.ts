import { Quentao } from './negocio/quentao';
import { connect } from 'mongoose';
import app from './app';

const url = 'mongodb+srv://dbUser:dbUser@cluster0-im9s2.mongodb.net/quentao?retryWrites=true&w=majority';

const main = async () => {
    try {
        const client = await connect(url, { useNewUrlParser: true });

        console.log(await Quentao.listarAtividades());
        app.listen(app.get('port'));

    } catch {
        console.log('deu brete');
    }
}

main();